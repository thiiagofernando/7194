using System.ComponentModel.DataAnnotations;

namespace Shop.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage="Este Campo é Obrigatório")]
        [MaxLength(60,ErrorMessage="Este Campo deve conter entre 3 e 60 caracteres")]
        [MinLength(3,ErrorMessage="Este Campo deve conter ente 3 e 60 caracteeres")]
        public string Title { get; set; }
    }
}