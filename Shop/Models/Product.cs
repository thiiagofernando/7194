using System.ComponentModel.DataAnnotations;

namespace Shop.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Este Campo é Obrigatório")]
        [MaxLength(60, ErrorMessage = "Este Campo deve conter entre 3 e 60 caracteres")]
        [MinLength(3, ErrorMessage = "Este Campo deve conter ente 3 e 60 caracteeres")]
        public string Title { get; set; }

        [MaxLength(1024, ErrorMessage = "Este campo dever conter no máximo 1024 caracteres")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Este Campo é Obrigatório")]
        [Range(1, int.MaxValue, ErrorMessage = "O Preço deve ser maior que zero")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Este Campo é Obrigatório")]
        [Range(1, int.MaxValue, ErrorMessage = "Categoria inválida")]
        public int CategoryId { get; set; }

        public Category Category { get; set; }

    }
}